# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}, "run_control": {"marked": true}}
from qiskit import BasicAer, execute
from time import time
from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister
from pprint import pprint
from qiskit import IBMQ
from qiskit.tools.monitor import job_monitor
import pixiedust
from time import time
from quantum_logic_operation import quantum_division

# + {"extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}}
# IBMQ.enable_account('f36ee70cf2ac474adb2005ccd48f0334019d4b78c0390e7f699938dd20398139582231fd01573b4f69b162e711353c453979831799e374db64acc60c1e301b42')
# IBMQ.save_account('f36ee70cf2ac474adb2005ccd48f0334019d4b78c0390e7f699938dd20398139582231fd01573b4f69b162e711353c453979831799e374db64acc60c1e301b42', overwrite=True)

# + {"extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}, "run_control": {"marked": true}}
float_qbit = 4
divisor_qbit = 2
dividend_qbit = 4
int_divisor_qbit, int_dividend_qbit = divisor_qbit, dividend_qbit
compare_qbit = 5
result_qbit = dividend_qbit - divisor_qbit + 1
dividend_qbit += float_qbit
result_qbit += float_qbit
all_qbit = divisor_qbit + dividend_qbit + compare_qbit + result_qbit
q_divisor = QuantumRegister(divisor_qbit, 'dsor')
q_dividend = QuantumRegister(dividend_qbit, 'ddend')
q_compare = QuantumRegister(compare_qbit, 'cmp')
q_result = QuantumRegister(result_qbit, 'res')
circuit = QuantumCircuit()
cr = ClassicalRegister(all_qbit, 'cr')

circuit.add_register(q_divisor)
circuit.add_register(q_dividend)
circuit.add_register(q_compare)
circuit.add_register(q_result)
all_component_lst = [q_divisor, q_dividend, q_compare, q_result]
qbit_lst = []
for qbit in all_component_lst:
    for i in range(qbit.size):
        qbit_lst.append(qbit[i])
# pprint(qbit_lst)
divisor_num = 4
dividend_num = 15
divisor_str = bin(divisor_num)[2:].zfill(int_divisor_qbit)
dividend_str = bin(dividend_num)[2:].zfill(int_dividend_qbit)
print(all_qbit)

# + {"extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}, "run_control": {"marked": true}}
num_str = divisor_str + dividend_str
for i in range(len(num_str)):
    if num_str[i] == '1':
        circuit.x(qbit_lst[i])
circuit.barrier()

# circuit.draw(output='mpl',scale=0.4,plot_barriers=False,filename='circuit-debug')

# + {"code_folding": [], "extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}, "hide_input": false, "pixiedust": {"displayParams": {}}}
# # %%pixie_debugger
quantum_division(circuit, qbit_lst, divisor_qbit, dividend_qbit, compare_qbit,
                 result_qbit)

# + {"extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}}
# circuit.draw(
#     output='mpl', scale=0.5, plot_barriers=False, filename='circuit-debug')

# + {"extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}}
circuit.add_register(cr)
for _ in range(all_qbit):
    circuit.measure(qbit_lst[_], cr[_])

# + {"extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}}
# circuit.draw(output='mpl',scale=0.6,plot_barriers=False,filename='circuit-debug')

# + {"extensions": {"jupyter_dashboards": {"version": 1, "views": {"grid_default": {}, "report_default": {}}}}}
shots = 1
# max_credits=3
# # IBMQ.load_accounts()
# backend=IBMQ.get_backend('ibmq_qasm_simulator')
# start=time()
# start
# job_exp = execute(circuit, backend, shots=shots, max_credits=max_credits)
# job_monitor(job_exp)
# end=time()
# print(end-start)
# result=job_exp.result()
# end=time()
# print(end-start)
# counts = result.get_counts(circuit)

backend = BasicAer.get_backend('qasm_simulator')
start = time()
result = execute(circuit, backend, shots=shots).result()
end = time()
print(end - start)
counts = result.get_counts(circuit)
all_lst = list(counts.keys())
print(all_lst[0])
num_str = all_lst[0]
int_part = int(num_str[float_qbit:result_qbit][::-1], 2)
if float_qbit > 0:
    float_str = num_str[:float_qbit][::-1]
    float_part = sum([
        pow(2, -(i + 1)) for i in range(len(float_str)) if float_str[i] == '1'
    ])
else:
    float_part = 0
    remainder = int(
        num_str[-(dividend_qbit + divisor_qbit):-divisor_qbit][::-1], 2)
    print('remainder:', remainder)
result = int_part + float_part
print('result:', result)

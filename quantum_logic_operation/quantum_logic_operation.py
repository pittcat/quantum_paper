#!/usr/bin/env python
# -*- coding: utf-8 -*-


def quantum_subtraction(circuit, qbit_lst, minuend_qbit,
                        reduction_qbit):  # reduction_qbit<minuend_qbit
    last_carry = reduction_qbit + minuend_qbit + 1
    cur_carry = reduction_qbit + minuend_qbit
    for i in range(reduction_qbit + minuend_qbit - 1, minuend_qbit - 1, -1):
        circuit.x(qbit_lst[i - reduction_qbit])
        circuit.cx(qbit_lst[last_carry], qbit_lst[i])
        circuit.ccx(qbit_lst[i - reduction_qbit], qbit_lst[i],
                    qbit_lst[cur_carry])
        circuit.x(qbit_lst[i - reduction_qbit])
        circuit.cx(qbit_lst[last_carry], qbit_lst[i])
        circuit.ccx(qbit_lst[last_carry], qbit_lst[i], qbit_lst[cur_carry])
        circuit.cx(qbit_lst[i], qbit_lst[i - reduction_qbit])
        circuit.cx(qbit_lst[last_carry], qbit_lst[i - reduction_qbit])
        circuit.reset(qbit_lst[last_carry])
        circuit.barrier()
        tmp = last_carry
        last_carry = cur_carry
        cur_carry = tmp

    if min(minuend_qbit, reduction_qbit) % 2 == 0:
        circuit.cx(qbit_lst[last_carry], qbit_lst[cur_carry])
        circuit.reset(qbit_lst[last_carry])
    circuit.barrier()
    if minuend_qbit != reduction_qbit:
        reduction_idx = reduction_qbit + minuend_qbit
        cur_carry = reduction_qbit + minuend_qbit + 1
        last_carry = reduction_qbit + minuend_qbit + 2
        for _ in range(minuend_qbit - reduction_qbit - 1, -1, -1):
            circuit.x(qbit_lst[_])
            circuit.cx(qbit_lst[last_carry], qbit_lst[reduction_idx])
            circuit.ccx(qbit_lst[_], qbit_lst[reduction_idx],
                        qbit_lst[cur_carry])
            circuit.x(qbit_lst[_])
            circuit.cx(qbit_lst[last_carry], qbit_lst[reduction_idx])
            circuit.ccx(qbit_lst[reduction_idx], qbit_lst[last_carry],
                        qbit_lst[cur_carry])
            circuit.cx(qbit_lst[reduction_idx], qbit_lst[_])
            circuit.cx(qbit_lst[last_carry], qbit_lst[_])
            circuit.barrier()
            circuit.reset(qbit_lst[reduction_idx])
            circuit.cx(qbit_lst[cur_carry], qbit_lst[reduction_idx])
            circuit.reset(qbit_lst[cur_carry])
            circuit.reset(qbit_lst[last_carry])


def quantum_adder(circuit, qbit_lst, adder_fir_qbit,
                  adder_sec_qbit):  # adder_sec_qbit<adder_fir_qbit
    last_carry = adder_fir_qbit + adder_sec_qbit
    cur_carry = last_carry + 1
    for _ in range(adder_fir_qbit + adder_sec_qbit - 1, adder_fir_qbit - 1,
                   -1):
        circuit.ccx(qbit_lst[_ - adder_sec_qbit], qbit_lst[_],
                    qbit_lst[cur_carry])
        circuit.cx(qbit_lst[_], qbit_lst[_ - adder_sec_qbit])
        circuit.ccx(qbit_lst[last_carry], qbit_lst[_ - adder_sec_qbit],
                    qbit_lst[cur_carry])
        circuit.cx(qbit_lst[last_carry], qbit_lst[_ - adder_sec_qbit])
        circuit.barrier()
        circuit.reset(qbit_lst[last_carry])
        circuit.barrier()
        tmp = last_carry
        last_carry = cur_carry
        cur_carry = tmp  # 进位结果在last_carry,cur_carry被置零

    if adder_fir_qbit != adder_sec_qbit:
        var_cur = last_carry
        for _ in range(adder_fir_qbit - adder_sec_qbit - 1, -1, -1):
            circuit.ccx(qbit_lst[var_cur], qbit_lst[_], qbit_lst[cur_carry])
            circuit.cx(qbit_lst[cur_carry], qbit_lst[_])
            circuit.barrier()
            circuit.reset(qbit_lst[var_cur])
            tmp = var_cur
            var_cur = cur_carry
            cur_carry = tmp

    #  进位结果存储到上面的辅助位
    up_carry = adder_fir_qbit + adder_sec_qbit
    down_carry = up_carry + 1
    if adder_fir_qbit % 2 == 1:
        circuit.cx(qbit_lst[down_carry], qbit_lst[up_carry])
        circuit.reset(qbit_lst[down_carry])


def quantum_division(circuit, qbit_lst, divisor_qbit, dividend_qbit,
                     compare_qbit, result_qbit):
    # %%pixie_debugger
    disdance = divisor_qbit
    result_idx = dividend_qbit + divisor_qbit + compare_qbit
    for i in range(2 * divisor_qbit - 1, 2 * divisor_qbit - 1 + result_qbit):
        #     比较结果
        comp_idx_last = 0
        comp_idx_cur = dividend_qbit + divisor_qbit
        com_idx_next = dividend_qbit + divisor_qbit + 2
        com_idx_assist = dividend_qbit + divisor_qbit + compare_qbit - 1
        c_cur = i
        for _ in range(divisor_qbit):
            circuit.x(qbit_lst[c_cur])
            circuit.ccx(qbit_lst[c_cur - disdance], qbit_lst[c_cur],
                        qbit_lst[comp_idx_cur])
            circuit.x(qbit_lst[c_cur - disdance])
            circuit.x(qbit_lst[c_cur])
            circuit.ccx(qbit_lst[c_cur - disdance], qbit_lst[c_cur],
                        qbit_lst[comp_idx_cur + 1])
            circuit.x(qbit_lst[c_cur - disdance])
            if _ != 0:
                circuit.barrier()
                circuit.x(qbit_lst[comp_idx_cur])
                circuit.x(qbit_lst[comp_idx_cur + 1])
                circuit.ccx(qbit_lst[comp_idx_cur], qbit_lst[comp_idx_cur + 1],
                            qbit_lst[com_idx_assist])
                circuit.x(qbit_lst[comp_idx_cur])
                circuit.x(qbit_lst[comp_idx_cur + 1])
                circuit.ccx(qbit_lst[comp_idx_last], qbit_lst[com_idx_assist],
                            qbit_lst[comp_idx_cur])
                circuit.ccx(qbit_lst[comp_idx_last + 1],
                            qbit_lst[com_idx_assist],
                            qbit_lst[comp_idx_cur + 1])
                circuit.barrier()
                circuit.reset(qbit_lst[comp_idx_last])
                circuit.reset(qbit_lst[comp_idx_last + 1])
                circuit.reset(qbit_lst[com_idx_assist])
            comp_idx_last = comp_idx_cur
            tmp = comp_idx_cur
            comp_idx_cur = com_idx_next
            com_idx_next = tmp
            circuit.barrier()
            c_cur -= 1
        circuit.barrier()
        # 一一对应
        res_cmp = dividend_qbit + divisor_qbit
        if divisor_qbit % 2 == 0:
            cmp_idx = dividend_qbit + divisor_qbit + 2
        else:
            cmp_idx = dividend_qbit + divisor_qbit

        circuit.x(qbit_lst[cmp_idx])
        if divisor_qbit % 2 == 0:
            circuit.cx(qbit_lst[cmp_idx], qbit_lst[res_cmp])  # 将结果存到第一条辅助比特

        # 储存单次结果到result_qbit
        circuit.barrier()
        circuit.cx(qbit_lst[res_cmp], qbit_lst[result_idx])

        circuit.barrier()
        if divisor_qbit % 2 == 0:
            circuit.reset(qbit_lst[cmp_idx])  # 重置复用保存比较结果的量子比特
        circuit.reset(qbit_lst[cmp_idx + 1])

        s_idx = dividend_qbit + divisor_qbit
        s_cur, s_ass, cur_carry, last_carry = i, s_idx + 1, s_idx + 3, s_idx + 4
        # 进行等位减法,s_cur是被减数，s_cur-disdance是减数,第一个辅助位s_idx是控制位，保存着比较结果，二三位为确定结果位----一一对应关系，四五位为进位和上一位进位，交替使用。
        for _ in range(divisor_qbit):
            circuit.cx(qbit_lst[s_idx], qbit_lst[s_cur])
            circuit.ccx(qbit_lst[s_idx], qbit_lst[last_carry],
                        qbit_lst[s_cur - disdance])
            circuit.barrier()

            # 第一次进位
            circuit.ccx(qbit_lst[s_cur], qbit_lst[s_cur - disdance],
                        qbit_lst[s_ass])
            circuit.barrier()
            circuit.ccx(qbit_lst[s_ass], qbit_lst[s_idx], qbit_lst[cur_carry])
            circuit.barrier()
            circuit.reset(qbit_lst[s_ass])

            circuit.barrier()
            circuit.cx(qbit_lst[s_idx], qbit_lst[s_cur])
            circuit.ccx(qbit_lst[s_idx], qbit_lst[last_carry],
                        qbit_lst[s_cur - disdance])

            # 第二次进位
            circuit.ccx(qbit_lst[s_cur - disdance], qbit_lst[last_carry],
                        qbit_lst[s_ass])
            circuit.barrier()
            circuit.ccx(qbit_lst[s_ass], qbit_lst[s_idx], qbit_lst[cur_carry])
            circuit.barrier()
            circuit.reset(qbit_lst[s_ass])

            #         给值
            circuit.barrier()
            circuit.ccx(qbit_lst[s_idx], qbit_lst[s_cur - disdance],
                        qbit_lst[s_cur])
            circuit.ccx(qbit_lst[s_idx], qbit_lst[last_carry], qbit_lst[s_cur])
            circuit.barrier()
            circuit.reset(qbit_lst[last_carry])
            tmp = last_carry
            last_carry = cur_carry
            cur_carry = tmp
            s_cur -= 1

        circuit.reset(qbit_lst[last_carry])

        circuit.reset(qbit_lst[s_idx])

        # 进行不等位相减之前的准备,由上一次对齐的第一位和比较器结果决定，需要上一次被减数的第一位为1，且a>b 成立--->第一个辅助位经过一个非门后为1
        # s_idx是第一个辅助位，s_cur+1是上一次被减数的第一位
        # 进行等位减法,s_cur是被减数，s_cur-disdance是减数,第一个辅助位s_idx是控制位，保存着比较结果，二三位为确定结果位----一一对应关系，四五位为进位和上一位进位，交替使用。
        if i < 2 * divisor_qbit - 1 + result_qbit - 1:
            circuit.barrier()
            circuit.cx(qbit_lst[s_cur + 1], qbit_lst[s_idx])

            # 储存结果到result_qbit+1
            circuit.barrier()
            circuit.cx(qbit_lst[s_idx], qbit_lst[result_idx + 1])
            circuit.cx(qbit_lst[s_idx], qbit_lst[s_cur + 1])

            # 进行不等位减法,d_cur是被减数，d_cur-disdance是减数,第一个辅助位d_idx是控制位，保存着比较结果，二三位为确定结果位----一一对应关系，四五位为进位和上一位进位，交替使用。
            d_idx = dividend_qbit + divisor_qbit
            d_disdance = disdance + 1
            d_cur, d_ass, cur_carry, last_carry = i + 1, d_idx + 1, d_idx + 3, d_idx + 4
            for _ in range(divisor_qbit):
                circuit.cx(qbit_lst[d_idx], qbit_lst[d_cur])
                circuit.ccx(qbit_lst[d_idx], qbit_lst[last_carry],
                            qbit_lst[d_cur - d_disdance])
                circuit.barrier()

                # 第一次进位
                circuit.ccx(qbit_lst[d_cur], qbit_lst[d_cur - d_disdance],
                            qbit_lst[d_ass])
                circuit.barrier()
                circuit.ccx(qbit_lst[d_ass], qbit_lst[d_idx],
                            qbit_lst[cur_carry])
                circuit.barrier()
                circuit.reset(qbit_lst[d_ass])

                circuit.barrier()
                circuit.cx(qbit_lst[d_idx], qbit_lst[d_cur])
                circuit.ccx(qbit_lst[d_idx], qbit_lst[last_carry],
                            qbit_lst[d_cur - d_disdance])

                # 第二次进位
                circuit.ccx(qbit_lst[d_cur - d_disdance], qbit_lst[last_carry],
                            qbit_lst[d_ass])
                circuit.barrier()
                circuit.ccx(qbit_lst[d_ass], qbit_lst[d_idx],
                            qbit_lst[cur_carry])
                circuit.barrier()
                circuit.reset(qbit_lst[d_ass])

                #         给值
                circuit.barrier()
                circuit.ccx(qbit_lst[d_idx], qbit_lst[d_cur - d_disdance],
                            qbit_lst[d_cur])
                circuit.ccx(qbit_lst[d_idx], qbit_lst[last_carry],
                            qbit_lst[d_cur])
                circuit.barrier()
                circuit.reset(qbit_lst[last_carry])
                tmp = last_carry
                last_carry = cur_carry
                cur_carry = tmp
                d_cur -= 1

            circuit.reset(qbit_lst[last_carry])

    #  下一次准备
        if i != 2 * divisor_qbit - 1 + result_qbit - 1:
            result_idx += 1
            disdance += 1
            circuit.reset(qbit_lst[d_idx])

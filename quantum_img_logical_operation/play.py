# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"run_control": {"marked": true}}
from qiskit import BasicAer, execute
from time import time
from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister
from pprint import pprint
import pixiedust

# + {"run_control": {"marked": true}}
divisor_qbit = 2
dividend_qbit = 4
compare_qbit = 5
result_qbit = 3
all_qbit = divisor_qbit + dividend_qbit + compare_qbit + result_qbit
q_divisor = QuantumRegister(divisor_qbit, 'dsor')
q_dividend = QuantumRegister(dividend_qbit, 'ddend')
q_compare = QuantumRegister(compare_qbit, 'cmp')
q_result = QuantumRegister(result_qbit, 'res')
circuit = QuantumCircuit()
cr = ClassicalRegister(all_qbit, 'cr')

circuit.add_register(q_divisor)
circuit.add_register(q_dividend)
circuit.add_register(q_compare)
circuit.add_register(q_result)
all_component_lst = [q_divisor, q_dividend, q_compare, q_result]
qbit_lst = []
for qbit in all_component_lst:
    for i in range(qbit.size):
        qbit_lst.append(qbit[i])
# pprint(qbit_lst)

# + {"run_control": {"marked": true}}
circuit.x(qbit_lst[0])
circuit.x(qbit_lst[1])

# circuit.x(qbit_lst[2])
# circuit.x(qbit_lst[3])
# circuit.x(qbit_lst[4])
circuit.x(qbit_lst[5])
circuit.barrier()
# circuit.draw(output='mpl',scale=0.4,plot_barriers=False,filename='circuit-debug')

# + {"pixiedust": {"displayParams": {}}, "code_folding": [], "hide_input": false}
# # %%pixie_debugger
disdance = 2
result_idx = dividend_qbit + divisor_qbit + compare_qbit
for i in range(3, 6):
    #     比较结果
    comp_idx_last = 0
    comp_idx_cur = dividend_qbit + divisor_qbit
    com_idx_next = dividend_qbit + divisor_qbit + 2
    com_idx_assist = dividend_qbit + divisor_qbit + compare_qbit - 1
    c_cur = i
    for _ in range(divisor_qbit):
        circuit.x(qbit_lst[c_cur])
        circuit.ccx(qbit_lst[c_cur - disdance], qbit_lst[c_cur],
                    qbit_lst[comp_idx_cur])
        circuit.x(qbit_lst[c_cur - disdance])
        circuit.x(qbit_lst[c_cur])
        circuit.ccx(qbit_lst[c_cur - disdance], qbit_lst[c_cur],
                    qbit_lst[comp_idx_cur + 1])
        circuit.x(qbit_lst[c_cur - disdance])
        if _ != 0:
            circuit.barrier()
            circuit.x(qbit_lst[comp_idx_cur])
            circuit.x(qbit_lst[comp_idx_cur + 1])
            circuit.ccx(qbit_lst[comp_idx_cur], qbit_lst[comp_idx_cur + 1],
                        qbit_lst[com_idx_assist])
            circuit.x(qbit_lst[comp_idx_cur])
            circuit.x(qbit_lst[comp_idx_cur + 1])
            circuit.ccx(qbit_lst[comp_idx_last], qbit_lst[com_idx_assist],
                        qbit_lst[comp_idx_cur])
            circuit.ccx(qbit_lst[comp_idx_last + 1], qbit_lst[com_idx_assist],
                        qbit_lst[comp_idx_cur + 1])
            circuit.barrier()
            circuit.reset(qbit_lst[comp_idx_last])
            circuit.reset(qbit_lst[comp_idx_last + 1])
            circuit.reset(qbit_lst[com_idx_assist])
        comp_idx_last = comp_idx_cur
        tmp = comp_idx_cur
        comp_idx_cur = com_idx_next
        com_idx_next = tmp
        circuit.barrier()
        c_cur -= 1
    circuit.barrier()
    # 一一对应
    ass_up = dividend_qbit + divisor_qbit
    ass_down = dividend_qbit + divisor_qbit + 1
    cmp_idx = dividend_qbit + divisor_qbit + 2

    circuit.x(qbit_lst[cmp_idx + 1])
    circuit.barrier()

    circuit.x(qbit_lst[ass_down])
    for _ in range(2):
        circuit.ccx(qbit_lst[ass_down], qbit_lst[cmp_idx + _],
                    qbit_lst[ass_up])
        circuit.reset(qbit_lst[ass_down])
        tmp = ass_down
        ass_down = ass_up
        ass_up = tmp
    circuit.cx(qbit_lst[ass_down],
               qbit_lst[ass_up])  # 把数值比较的结果赋值到第一条,只有a>b时，这条量子比特的输出才为1。
    circuit.x(qbit_lst[ass_up])
    circuit.reset(qbit_lst[ass_down])
    circuit.reset(qbit_lst[cmp_idx])  # 重置复用保存比较结果的第三和第四条量子比特
    circuit.reset(qbit_lst[cmp_idx + 1])

    # 储存单次结果到result_qbit
    circuit.barrier()
    circuit.cx(qbit_lst[ass_up], qbit_lst[result_idx])

    s_idx = dividend_qbit + divisor_qbit
    s_cur, s_ass_up, s_ass_down, cur_carry, last_carry = i, s_idx + 1, s_idx + 2, s_idx + 3, s_idx + 4
    # 进行等位减法,s_cur是被减数，s_cur-disdance是减数,第一个辅助位s_idx是控制位，保存着比较结果，二三位为确定结果位----一一对应关系，四五位为进位和上一位进位，交替使用。
    for _ in range(divisor_qbit):
        circuit.cx(qbit_lst[s_idx], qbit_lst[s_cur])
        circuit.ccx(qbit_lst[s_idx], qbit_lst[last_carry],
                    qbit_lst[s_cur - disdance])
        circuit.x(qbit_lst[s_ass_down])  # 一一对应初始化
        for x in [s_cur, s_cur - disdance, s_idx]:
            circuit.barrier()
            circuit.ccx(qbit_lst[x], qbit_lst[s_ass_down], qbit_lst[s_ass_up])
            circuit.reset(qbit_lst[s_ass_down])
            tmp = s_ass_down
            s_ass_down = s_ass_up
            s_ass_up = tmp
        circuit.barrier()
        circuit.cx(qbit_lst[s_idx + 1], qbit_lst[cur_carry])  # 第一次进位
        circuit.reset(qbit_lst[s_idx + 1])
        s_ass_up, s_ass_down = s_idx + 1, s_idx + 2

        circuit.barrier()
        circuit.cx(qbit_lst[s_idx], qbit_lst[s_cur])
        circuit.ccx(qbit_lst[s_idx], qbit_lst[last_carry],
                    qbit_lst[s_cur - disdance])

        circuit.barrier()
        circuit.x(qbit_lst[s_ass_down])  # 一一对应初始化
        for x in [s_cur - disdance, last_carry, s_idx]:
            circuit.barrier()
            circuit.ccx(qbit_lst[x], qbit_lst[s_ass_down], qbit_lst[s_ass_up])
            circuit.reset(qbit_lst[s_ass_down])
            tmp = s_ass_down
            s_ass_down = s_ass_up
            s_ass_up = tmp
        circuit.barrier()
        circuit.cx(qbit_lst[s_idx + 1], qbit_lst[cur_carry])  # 第二次进位
        circuit.reset(qbit_lst[s_idx + 1])
        s_ass_up, s_ass_down = s_idx + 1, s_idx + 2
        #         给值
        circuit.barrier()
        circuit.ccx(qbit_lst[s_idx], qbit_lst[s_cur - disdance],
                    qbit_lst[s_cur])
        circuit.ccx(qbit_lst[s_idx], qbit_lst[last_carry], qbit_lst[s_cur])
        circuit.barrier()
        circuit.reset(qbit_lst[last_carry])
        tmp = last_carry
        last_carry = cur_carry
        cur_carry = tmp
        s_cur -= 1
    circuit.reset(qbit_lst[last_carry])

    # 进行不等位相减之前的准备,由上一次对齐的第一位和比较器结果决定，需要上一次被减数的第一位为1，且a>b 成立--->第一个辅助位经过一个非门后为1
    # s_idx是第一个辅助位，s_cur+1是上一次被减数的第一位
    # 进行等位减法,s_cur是被减数，s_cur-disdance是减数,第一个辅助位s_idx是控制位，保存着比较结果，二三位为确定结果位----一一对应关系，四五位为进位和上一位进位，交替使用。
    if i < 5:
        circuit.barrier()
        circuit.x(qbit_lst[s_idx])
        circuit.ccx(qbit_lst[s_idx], qbit_lst[s_cur + 1], qbit_lst[s_idx + 1])
        circuit.reset(qbit_lst[s_idx])
        circuit.cx(qbit_lst[s_idx + 1], qbit_lst[s_idx])
        circuit.reset(qbit_lst[s_idx + 1])
        circuit.barrier()

        # 储存结果到result_qbit+1
        circuit.barrier()
        circuit.cx(qbit_lst[s_idx], qbit_lst[result_idx + 1])
        circuit.cx(qbit_lst[s_idx],qbit_lst[s_cur+1])

        # 进行不等位减法,d_cur是被减数，d_cur-disdance是减数,第一个辅助位d_idx是控制位，保存着比较结果，二三位为确定结果位----一一对应关系，四五位为进位和上一位进位，交替使用。
        d_idx = dividend_qbit + divisor_qbit
        d_disdance = disdance + 1
        d_cur, d_ass_up, d_ass_down, cur_carry, last_carry = i + 1, d_idx + 1, d_idx + 2, d_idx + 3, d_idx + 4
        for _ in range(divisor_qbit):
            circuit.cx(qbit_lst[d_idx], qbit_lst[d_cur])
            circuit.ccx(qbit_lst[d_idx], qbit_lst[last_carry],
                        qbit_lst[d_cur - d_disdance])
            circuit.x(qbit_lst[d_ass_down])  # 一一对应初始化
            for x in [d_cur, d_cur - d_disdance, d_idx]:
                circuit.barrier()
                circuit.ccx(qbit_lst[x], qbit_lst[d_ass_down],
                            qbit_lst[d_ass_up])
                circuit.reset(qbit_lst[d_ass_down])
                tmp = d_ass_down
                d_ass_down = d_ass_up
                d_ass_up = tmp
            circuit.barrier()
            circuit.cx(qbit_lst[d_idx + 1], qbit_lst[cur_carry])  # 第一次进位
            circuit.reset(qbit_lst[d_idx + 1])
            d_ass_up, d_ass_down = d_idx + 1, d_idx + 2

            circuit.barrier()
            circuit.cx(qbit_lst[d_idx], qbit_lst[d_cur])
            circuit.ccx(qbit_lst[d_idx], qbit_lst[last_carry],
                        qbit_lst[d_cur - d_disdance])

            circuit.barrier()
            circuit.x(qbit_lst[d_ass_down])  # 一一对应初始化
            for x in [d_cur - d_disdance, last_carry, d_idx]:
                circuit.barrier()
                circuit.ccx(qbit_lst[x], qbit_lst[d_ass_down],
                            qbit_lst[d_ass_up])
                circuit.reset(qbit_lst[d_ass_down])
                tmp = d_ass_down
                d_ass_down = d_ass_up
                d_ass_up = tmp
            circuit.barrier()
            circuit.cx(qbit_lst[d_idx + 1], qbit_lst[cur_carry])  # 第二次进位
            circuit.reset(qbit_lst[d_idx + 1])
            d_ass_up, d_ass_down = d_idx + 1, d_idx + 2
            #         给值
            circuit.barrier()
            circuit.ccx(qbit_lst[d_idx], qbit_lst[d_cur - d_disdance],
                        qbit_lst[d_cur])
            circuit.ccx(qbit_lst[d_idx], qbit_lst[last_carry], qbit_lst[d_cur])
            circuit.barrier()
            circuit.reset(qbit_lst[last_carry])
            tmp = last_carry
            last_carry = cur_carry
            cur_carry = tmp
            d_cur -= 1
        circuit.reset(qbit_lst[last_carry])

#  下一次准备
    result_idx += 1
    disdance += 1
    circuit.reset(qbit_lst[s_idx])
# 一个数相减的时候没得问题，两个数相减的时候为什么第二个数为1
# -

circuit.draw(
    output='mpl', scale=0.5, plot_barriers=False, filename='circuit-debug')

circuit.add_register(cr)
for _ in range(all_qbit):
    circuit.measure(qbit_lst[_], cr[_])

# +
# circuit.draw(output='mpl',scale=0.4,plot_barriers=False,filename='circuit-debug')
# -

backend = BasicAer.get_backend('qasm_simulator')
result = execute(circuit, backend, shots=1).result()
counts = result.get_counts(circuit)
all_lst = list(counts.keys())
print(all_lst[0])
num_str=all_lst[0]
result=int(num_str[:3][::-1],2)
remainder=int(num_str[-(dividend_qbit+divisor_qbit):-divisor_qbit][::-1],2)
# print(num_str[:3][::-1])
# print(num_str[-dividend_qbit:-divisor_qbit][::-1])
print('result:',result)
print('remainder:',remainder)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
#  import cv2
import os
import png


def image_gen(scope, dimension, suffix=''):
    #  Generate the input image
    image_matrix = np.random.randint(
        scope, size=(dimension, dimension)).astype('u1')
    if suffix is not '':
        suffix = '-' + suffix
    file_name = os.path.dirname(os.path.realpath(
        __file__)) + '/' + 'source' + '-' + str(dimension) + suffix + '.png'
    png.from_array(image_matrix, 'L').save(file_name)


def main():
    dimensions = [pow(2, x) for x in range(1, 4)]
    scope = 8
    for dimension in dimensions:
        image_gen(scope, dimension, 'minuend')
        image_gen(scope, dimension, 'reduction')


if __name__ == "__main__":
    main()

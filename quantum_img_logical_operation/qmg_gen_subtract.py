#!/usr/bin/env python
# -*- coding: utf-8 -*-


def qmg_gen_subtract(circuit, qr, color_n, target_state_fir, target_state_sec,
                     state_length, reg_counts):
    """
    generate a qmg for image subtraction,one positon info corresponds to two gray level information
    """
    #  add x gate to value is zero
    pos_record_idx = []
    pos_idx = state_length + color_n - 1
    for j in target_state_fir[color_n:state_length]:
        if j == '0':
            circuit.x(qr[pos_idx])
            pos_record_idx.append(pos_idx)
        pos_idx -= 1

    circuit.x(qr[reg_counts - 1])
    control_idx = reg_counts - 2
    zero_idx = reg_counts - 1
    for pos_cur in range(2 * color_n, state_length + color_n):
        circuit.ccx(qr[pos_cur], qr[zero_idx], qr[control_idx])
        circuit.reset(qr[zero_idx])
        tmp = control_idx
        control_idx = zero_idx
        zero_idx = tmp

    color_pos = 0
    for state in target_state_fir[:color_n] + target_state_sec[:color_n]:
        if state == '1':
            circuit.cx(qr[zero_idx], qr[color_pos])
        color_pos += 1
    circuit.reset(qr[zero_idx])

    #  add x lock according to pos_record_idx
    for pos in pos_record_idx:
        circuit.x(qr[pos])

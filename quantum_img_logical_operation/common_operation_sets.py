#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import os
import matplotlib.pyplot as plt

def show_normal_image(image):
    """TODO: Docstring for show_normal_image.

    :image: matrix
    :returns: None

    """
    image_matrix = cv2.imread(image, 0)
    plt.axis('off')
    plt.gray()
    plt.imshow(image_matrix)
    plt.show()


def transpose_image(image):
    """TODO: Docstring for transpose_image.

    :image: file name
    :returns: None

    """
    image_matrix = cv2.imread(
        os.path.dirname(os.path.realpath(__file__)) + '/' + image, 0)
    out_file = os.path.dirname(os.path.realpath(__file__)) + '/' + image.split(
        '.')[0] + '-transpose' + '.png'
    cv2.imwrite(out_file, image_matrix.T)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from image2neqrstr import image2neqrstring


def neqr_info(color_n, files, dirname):
    file_path = os.path.dirname(
        os.path.realpath(__file__)) + '/' + dirname + '/'
    file_lst = [file_path + file_name for file_name in files]
    neqr_strs = image2neqrstring(color_n, file_lst)
    neqr_str = neqr_strs[0][0]
    state_length = len(neqr_str)
    return state_length, neqr_strs


def qstate2circuit_by_record_toffoli(circuit, qr, color_n, target_state,
                                     state_length, reg_counts):
    #  add x gate to value is zero
    pos_record_idx = []
    pos_idx = state_length - 1
    for j in target_state[color_n:state_length][::-1]:
        if j == '0':
            circuit.x(qr[pos_idx])
            pos_record_idx.append(pos_idx)
        pos_idx -= 1

    circuit.x(qr[reg_counts - 1])
    control_idx = reg_counts - 2
    zero_idx = reg_counts - 1
    for pos_cur in range(color_n, state_length):
        circuit.ccx(qr[pos_cur], qr[zero_idx], qr[control_idx])
        circuit.reset(qr[zero_idx])
        tmp = control_idx
        control_idx = zero_idx
        zero_idx = tmp

    color_pos = color_n - 1
    for state in target_state[:color_n][::-1]:
        if state == '1':
            circuit.cx(qr[zero_idx], qr[color_pos])
        color_pos -= 1
    circuit.reset(qr[zero_idx])

    #  add x lock according to pos_record_idx
    for pos in pos_record_idx:
        circuit.x(qr[pos])

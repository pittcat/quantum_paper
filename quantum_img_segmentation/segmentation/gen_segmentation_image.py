import numpy as np
import cv2
import os
import png


def image_gen(scope, dimension, min_vpt, max_vpt):
    #  Generate the input image
    in_image_matrix = np.random.randint(
        scope, size=(dimension, dimension)).astype('u1')
    in_file = os.path.dirname(os.path.realpath(
        __file__)) + '/' + 'in' + '-' + str(dimension) + '.png'
    png.from_array(in_image_matrix, 'L').save(in_file)
    #  Image segmentation
    image = cv2.imread(in_file, 0)
    out_image_matrix = image.astype('u1')
    shape_size = out_image_matrix.shape[0]
    for x in range(shape_size):
        for y in range(shape_size):
            if not (out_image_matrix[x][y] >= min_vpt
                    and out_image_matrix[x][y] <= max_vpt):
                out_image_matrix[x][y] = 0

    #  Generate the out image
    out_file = os.path.dirname(os.path.realpath(
        __file__)) + '/' + 'out' + '-' + str(dimension) + '.png'
    png.from_array(out_image_matrix, 'L').save(out_file)


def main():
    dimensions = [pow(2, x) for x in range(1, 4)]
    scope = 255
    min_vpt = 0
    max_vpt = 255
    for dimension in dimensions:
        image_gen(scope, dimension, min_vpt, max_vpt)


if __name__ == "__main__":
    main()

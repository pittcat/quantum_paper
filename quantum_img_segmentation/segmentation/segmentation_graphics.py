import numpy as np
import random
import cv2
import os
import png
from pprint import pprint


def img_side_graphics(scope, dimension, min_vpt, max_vpt):
    impurity_lst = list(range(0,165))+list(range(181,256))
    graphics_lst = list(range(165,181))
    side_graphics_pos =[]

    graphics_side=dimension//2
    graphics_indent=dimension//4
    first_idx=pow(dimension,2)//4+graphics_indent
    last_idx=pow(dimension,2)*3//4-dimension+graphics_indent
    print(first_idx,last_idx)
    side_graphics_pos+=list(range(first_idx,first_idx+graphics_side))+list(range(last_idx,last_idx+graphics_side))
    opposite_angles_pos=[]
    opposite_angles_idx=first_idx+graphics_side-1
    for _ in range(0,graphics_side-2):
        opposite_angles_idx+=dimension
        opposite_angles_idx-=1
        opposite_angles_pos.append(opposite_angles_idx)
    
    side_graphics_pos+=opposite_angles_pos

    in_image_matrix = []
    for i in range(pow(dimension, 2)):
        in_image_matrix.append(random.choice(impurity_lst))

    for pos in side_graphics_pos:
        in_image_matrix[pos] = random.choice(graphics_lst)
    #  Generate the input image
    in_image_matrix = np.array(in_image_matrix)
    shape = (dimension, dimension)
    in_image_matrix = in_image_matrix.reshape(shape).astype('u1')
    pprint(in_image_matrix)

    in_file = os.path.dirname(os.path.realpath(
        __file__)) + '/' + 'in' + '-' + str(dimension) + '-graphics' + '.png'
    png.from_array(in_image_matrix, 'L').save(in_file)
    #  Image segmentation
    image = cv2.imread(in_file, 0)
    out_image_matrix = image.astype('u1')
    shape_size = out_image_matrix.shape[0]
    for x in range(shape_size):
        for y in range(shape_size):
            if not (out_image_matrix[x][y] >= min_vpt
                    and out_image_matrix[x][y] <= max_vpt):
                out_image_matrix[x][y] = 0

    #  Generate the out image
    out_file = os.path.dirname(os.path.realpath(
        __file__)) + '/' + 'out' + '-' + str(dimension) + '-graphics' + '.png'
    png.from_array(out_image_matrix, 'L').save(out_file)


def main():
    dimensions = [pow(2, x) for x in range(3, 7)]
    scope = 8
    min_vpt = 165
    max_vpt = 180
    for dimension in dimensions:
        img_side_graphics(scope, dimension, min_vpt, max_vpt)


if __name__ == "__main__":
    main()
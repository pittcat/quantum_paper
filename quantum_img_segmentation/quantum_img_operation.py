#!/usr/bin/env python
# -*- coding: utf-8 -*-

from neqr_preparation_record_toffoli_v2 import qstate2circuit_by_record_toffoli, neqr_info
from qiskit import QuantumCircuit, QuantumRegister
from numpy import binary_repr


def add_comparator(color_n, vpt, qr, c_qr, qr_lst, circuit, init_idx):
    #  add quantum register
    vpt_bin = binary_repr(vpt) if len(binary_repr(vpt)) == color_n else (
        color_n - len(binary_repr(vpt))) * '0' + binary_repr(vpt)
    #  init comparative number
    cur = init_idx
    for i in vpt_bin:
        if i == '1':
            circuit.x(qr_lst[cur])
        cur += 1
    pos_n = init_idx - color_n
    up_cur_idx = color_n - 1
    res_cur = len(qr_lst) - 5
    final_cur = len(qr_lst) - 1
    for i in range(color_n):
        down_cur_idx = up_cur_idx + pos_n + color_n
        circuit.x(qr_lst[down_cur_idx])
        circuit.ccx(qr_lst[up_cur_idx], qr_lst[down_cur_idx], qr_lst[res_cur])
        circuit.x(qr_lst[down_cur_idx])
        circuit.x(qr_lst[up_cur_idx])
        circuit.ccx(qr_lst[up_cur_idx], qr_lst[down_cur_idx],
                    qr_lst[res_cur + 1])
        circuit.x(qr_lst[up_cur_idx])
        up_cur_idx -= 1
        if i == 0:
            res_cur += 2
            continue
        else:
            if i % 2 == 1:
                circuit.x(qr_lst[res_cur])
                circuit.x(qr_lst[res_cur + 1])
                circuit.ccx(qr_lst[res_cur], qr_lst[res_cur + 1],
                            qr_lst[final_cur])
                circuit.x(qr_lst[res_cur])
                circuit.x(qr_lst[res_cur + 1])
                circuit.ccx(qr_lst[final_cur], qr_lst[res_cur - 2],
                            qr_lst[res_cur])
                circuit.ccx(qr_lst[final_cur], qr_lst[res_cur - 1],
                            qr_lst[res_cur + 1])
                circuit.reset(qr_lst[final_cur])
                circuit.reset(qr_lst[res_cur - 2])
                circuit.reset(qr_lst[res_cur - 1])
                res_cur -= 2
            else:
                circuit.x(qr_lst[res_cur])
                circuit.x(qr_lst[res_cur + 1])
                circuit.ccx(qr_lst[res_cur], qr_lst[res_cur + 1],
                            qr_lst[final_cur])
                circuit.x(qr_lst[res_cur])
                circuit.x(qr_lst[res_cur + 1])
                circuit.ccx(qr_lst[final_cur], qr_lst[res_cur + 2],
                            qr_lst[res_cur])
                circuit.ccx(qr_lst[final_cur], qr_lst[res_cur + 3],
                            qr_lst[res_cur + 1])
                circuit.reset(qr_lst[final_cur])
                circuit.reset(qr_lst[res_cur + 2])
                circuit.reset(qr_lst[res_cur + 3])
                res_cur += 2
    return circuit


def main():
    #  generate quantum image
    files = ['in-2.png', 'out-2.png']
    color_n = 3
    state_length, all_quantum_states = neqr_info(color_n, files,
                                                 'segmentation')
    assist_n = 2
    reg_counts = state_length + assist_n

    qr = QuantumRegister(reg_counts, 'qr')
    circuit = QuantumCircuit(qr)
    for i in range(color_n, state_length):
        circuit.h(qr[i])

    for target_state in all_quantum_states[0]:
        qstate2circuit_by_record_toffoli(circuit, qr, color_n, target_state,
                                         state_length, reg_counts)

    #  add quantum register for segmentation
    c_qr = QuantumRegister(color_n + 3, 'c_qr')
    circuit.add_register(c_qr)
    qr_lst = [qr[i]
              for i in range(qr.size)] + [c_qr[i] for i in range(c_qr.size)]
    if color_n % 2 == 1:
        state_cur = len(qr_lst) - 5
        judge_cur = len(qr_lst) - 3
    else:
        state_cur = len(qr_lst) - 3
        judge_cur = len(qr_lst) - 5

    #  add max vpt
    circuit = add_comparator(color_n, 5, qr, c_qr, qr_lst, circuit,
                             state_length)
    circuit.x(qr_lst[state_cur + 1])
    circuit.ccx(qr_lst[state_cur], qr_lst[state_cur + 1], qr_lst[judge_cur])
    for i in range(color_n):
        circuit.cx(qr_lst[i], qr_lst[-1])
        circuit.ccx(qr_lst[-1], qr_lst[judge_cur], qr_lst[i])
        circuit.reset

    # reset comparand number
    cur = state_length
    for i in range(color_n):
        circuit.reset(qr_lst[cur])
        cur += 1
    cur = state_length
    for i in range(color_n):
        circuit.reset(qr_lst[cur])
        cur += 1
    # reset all comparison circuit
    circuit.reset(qr_lst[judge_cur])
    circuit.reset(qr_lst[state_cur])
    circuit.reset(qr_lst[state_cur + 1])
    # add min vpt
    circuit = add_comparator(color_n, 2, qr, c_qr, qr_lst, circuit,
                             state_length)
    circuit.x(qr_lst[state_cur])
    circuit.ccx(qr_lst[state_cur], qr_lst[state_cur + 1], qr_lst[judge_cur])
    for i in range(color_n):
        circuit.cx(qr_lst[i], qr_lst[-1])
        circuit.ccx(qr_lst[-1], qr_lst[judge_cur], qr_lst[i])
        circuit.reset(qr_lst[-1])(qr_lst[-1])


if __name__ == "__main__":
    main()

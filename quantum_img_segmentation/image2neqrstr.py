#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import os
from math import log2
import numpy as np


def image2neqrstring(gray_length, file_lst):
    quan_states_lst = []
    image_lst = [cv2.imread(file, 0) for file in file_lst]
    for image in image_lst:
        quan_states = []
        image_matrix = image.astype('u1')
        shape_size = image_matrix.shape[0]
        pos_size = int(log2(shape_size))
        for Y in range(shape_size):
            for X in range(shape_size):
                expr_y, expr_x, gray_v = np.binary_repr(Y), np.binary_repr(
                    X), np.binary_repr(image_matrix[Y][X])
                if len(expr_x) != pos_size:
                    expr_x = (pos_size - len(expr_x)) * '0' + expr_x
                if len(expr_y) != pos_size:
                    expr_y = (pos_size - len(expr_y)) * '0' + expr_y
                if len(gray_v) != gray_length:
                    gray_v = (gray_length - len(gray_v)) * '0' + gray_v
                quan_states.append(gray_v + expr_y + expr_x)
        quan_states_lst.append(quan_states)
    return quan_states_lst


def main():
    file_path = os.path.dirname(os.path.realpath(__file__)) + '/segmentation/'
    file_lst = [
        file_path + file_name for file_name in ['in-2.png', 'out-2.png']
    ]
    res = image2neqrstring(3, file_lst)
    print(res)


if __name__ == "__main__":
    main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import os
import png


def image_gen(scope, dimension, min_vpt, max_vpt):
    #  Generate the input image
    in_image_matrix = np.random.randint(
        scope, size=(dimension, dimension)).astype('u1')
    in_file = os.path.dirname(os.path.realpath(
        __file__)) + '/' + 'in' + '-' + str(dimension) + '.png'
    png.from_array(in_image_matrix, 'L').save(in_file)

def main():
    dimensions = [pow(2, x) for x in range(1, 6)]
    scope = 255
    min_vpt = 0
    max_vpt = 255
    for dimension in dimensions:
        image_gen(scope, dimension, min_vpt, max_vpt)


if __name__ == "__main__":
    main()

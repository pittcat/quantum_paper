#!/usr/bin/env python
# -*- coding=utf-8 -*-

from qiskit import QuantumCircuit, QuantumRegister
from qiskit import Aer, transpile
from common_func_sets import counts2lst, encrypt

c1 = QuantumRegister(4, 'c1')
t = QuantumRegister(1, 't')
res = QuantumRegister(2, 'res')
circ = QuantumCircuit(c1, t, res)

circ.h(c1)
circ.barrier()
circ.cx(c1[2], t)
circ.cx(c1[0], t)
circ.barrier()

circ.x(res[0])
circ.ccx(t, res[0], res[1])
circ.reset(t)
circ.reset(res[0])
circ.barrier()

circ.cx(c1[3], t)
circ.cx(c1[1], t)
circ.barrier()

circ.ccx(res[1], t, res[0])
circ.reset(t)
circ.reset(res[1])
circ.barrier()

circ.measure_all()

# run circuit on local simulator
simulator = Aer.get_backend('aer_simulator')
circ = transpile(circ, simulator)
result = simulator.run(circ, shots=30).result()
counts = result.get_counts(circ)

# convert counts to list
counts_lst = counts2lst(counts)
for _ in counts_lst:
    print(encrypt(_, 2))

circ.draw(output='mpl', filename='xor.png', scale=0.5, fold=50)

from qiskit import QuantumCircuit, QuantumRegister


def gen_quantum_circuit(gray_size, pos_size, template_size):
    """
    :function: init quantum circuit
    :gray_size: the number of qbits that represent image gray info
    :pos_size: the number of qbits that represent image position info
    :template_size: the number of qbits that represent template image position info
    :returns: quantum circuit
    """
    q_gray1 = QuantumRegister(gray_size, 'qg_up')
    q_gray2 = QuantumRegister(gray_size, 'qg_down')
    q_pos = QuantumRegister(pos_size, 'qp')
    q_template_pos1 = QuantumRegister(template_size, 'qtp_up')  # y--xx
    q_template_pos2 = QuantumRegister(template_size, 'qtp_down')  # y--xx
    q_init_ass = QuantumRegister(2, 'qia')
    q_template_ass = QuantumRegister(2, 'qta')
    return QuantumCircuit(q_gray1, q_gray2, q_pos, q_template_pos1,
                          q_template_pos2, q_init_ass, q_template_ass)


def pos_correspondence(circuit, pos_str, pos_lst, pos_assist_lst):
    """
    :function: let the position information be loaded into a quantum bit.
    :circuit: quantum circuit after initialization
    :pos_str: binary string that represent position
    :pos_lst: list of circuit indexes that represent position info qbits
    :pos_assist_lst: list of circuit indexes that represent auxiliary qbits
    :return: quantum circuit
    """
    pos_assist_cur, pos_assist_next = pos_assist_lst[0], pos_assist_lst[1]
    for i, j in zip(pos_str, pos_lst):
        if i == '0':
            circuit.x(j)
            circuit.ccx(j, pos_assist_cur, pos_assist_next)
            circuit.x(j)
            circuit.barrier(pos_lst)
        else:
            circuit.ccx(j, pos_assist_cur, pos_assist_next)
        circuit.reset(pos_assist_cur)
        pos_assist_cur, pos_assist_next = pos_assist_next, pos_assist_cur

    return circuit


def fix_gray_info(circuit, gray_str, gray_info_lst, pos_info, tpost_info):
    """
    :function: fix gray info to qbits
    :circuit: quantum circuit where posision info has been fixed
    :gray_info_lst: list of indexes that represent gray info in quantum circuit
    :gray_str: binary string that represents image gray info
    :pos_info: binary string that represents original image posision info
    :tpost_info: binary string that represents template image posision info
    :return: return circuit
    """
    for i, j in zip(gray_str, gray_info_lst):
        if i == '1':
            circuit.ccx(pos_info, tpost_info, j)
    return circuit


def reset_origin_auxiliary(circuit, auxiliary_idx):
    """
    :function: take operation to auxiliary qbits
    :circuit: quantum circuit
    :auxiliary_idx: index list of auxiliary qbits
    :return: circuit
    """
    for _ in auxiliary_idx:
        circuit.reset(_)
    circuit.x(auxiliary_idx[-1])
    return circuit

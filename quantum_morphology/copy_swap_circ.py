#!/usr/bin/env python
# -*- coding=utf-8 -*-

from qiskit import QuantumCircuit, QuantumRegister
from common_func_sets import (gen_position_info, array2neqrstring, counts2lst,
                              encrypt, copy_gray)
from qiskit import Aer, transpile
import numpy as np

#  The grayscale information, from top to bottom,
#  corresponds to the binary from high to low

image_matrix = np.array([[1, 2], [3, 0]])

print(gen_position_info(image_matrix))
print(array2neqrstring(image_matrix, gray_length=2, pos_size=1))

c_gray1 = QuantumRegister(2, 'cgray1')
c_gray2 = QuantumRegister(2, 'cgray2')
gray = QuantumRegister(2, 'gray')
pos = QuantumRegister(2, 'pos')
apos = QuantumRegister(2, 'apos')
assist = QuantumRegister(5, "assist")
# circ = QuantumCircuit(c_gray1, c_gray2, gray, pos, apos, assist)
circ = QuantumCircuit(c_gray1, c_gray2, gray, pos, apos, assist)
circ.h(pos)

circ.barrier()
#  ['0100', '1001', '1110', '0011']
#  00 pos
circ.x(pos)
#  circ.ccx(pos[0],pos[1],gray[0])
circ.ccx(pos[0], pos[1], gray[1])
circ.x(pos)
circ.barrier()

#  01
circ.x(pos[0])
circ.ccx(pos[0], pos[1], gray[0])
circ.x(pos[0])
circ.barrier()

#  10
circ.x(pos[1])
circ.ccx(pos[0], pos[1], gray[0])
circ.ccx(pos[0], pos[1], gray[1])
circ.x(pos[1])
circ.barrier()

#  00
# circ.ccx(pos[0], pos[1], gray[1])
circ.barrier()

# copy value

## init apos
circ.h(apos)

## pos sync with apos
circ.cx(pos[0], assist[0])
circ.cx(apos[0], assist[0])
circ.barrier()

circ.cx(pos[1], assist[1])
circ.cx(apos[1], assist[1])
circ.barrier()

circ.ccx(assist[0], assist[1], assist[2])

circ.reset([assist[0], assist[1]])
circ.barrier()


circ.x(pos[0])
circ.ccx(pos[0],pos[1],assist[0])
circ.ccx(assist[0],gray[0],c_gray1[0])
circ.ccx(assist[0],gray[1],c_gray1[1])
circ.x(pos[0])
circ.barrier()

# # swap
circ.cswap(assist[2], pos[0], apos[0])
circ.cswap(assist[2], pos[1], apos[1])

### copy apos sync value

circ.measure_all()

# run circuit on IBMQ
# counts = get_quantum_circuit_out(circ, shot_times=400)

# run circuit on local simulator
simulator = Aer.get_backend('aer_simulator')
circ = transpile(circ, simulator)
result = simulator.run(circ, shots=100).result()
counts = result.get_counts(circ)

counts_lst = counts2lst(counts)
print(counts_lst)

print("counts of list:", len(counts_lst))

print("all res state:")
for _ in counts_lst:
    print(encrypt(_, 2))

circ.draw(output='mpl', filename='copy_swap_circ.png', scale=0.5, fold=50)

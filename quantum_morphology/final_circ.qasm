OPENQASM 2.0;
include "qelib1.inc";
qreg tv1[4];
qreg tv2[4];
qreg qg[4];
qreg qp[4];
qreg qtp[4];
qreg qia[2];
qreg qta[2];
creg meas[24];
h qp[0];
h qp[1];
h qp[2];
h qp[3];
h qtp[0];
h qtp[1];
h qtp[2];
h qtp[3];
x qia[0];
x qta[0];
x qp[0];
ccx qp[0],qia[0],qia[1];
x qp[0];
barrier qp[0],qp[1],qp[2],qp[3];
reset qia[0];
ccx qp[1],qia[1],qia[0];
reset qia[1];
x qp[2];
ccx qp[2],qia[0],qia[1];
x qp[2];
barrier qp[0],qp[1],qp[2],qp[3];
reset qia[0];
ccx qp[3],qia[1],qia[0];
reset qia[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
x qtp[0];
ccx qtp[0],qta[0],qta[1];
x qtp[0];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
x qtp[1];
ccx qtp[1],qta[1],qta[0];
x qtp[1];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
x qtp[2];
ccx qtp[2],qta[0],qta[1];
x qtp[2];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
x qtp[3];
ccx qtp[3],qta[1],qta[0];
x qtp[3];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[0],qta[0],qg[1];
ccx qia[0],qta[0],qg[2];
ccx qia[0],qta[0],qg[3];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
x qtp[0];
ccx qtp[0],qta[0],qta[1];
x qtp[0];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
x qtp[1];
ccx qtp[1],qta[1],qta[0];
x qtp[1];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
x qtp[2];
ccx qtp[2],qta[0],qta[1];
x qtp[2];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
ccx qtp[3],qta[1],qta[0];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[0],qta[0],qg[0];
ccx qia[0],qta[0],qg[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
x qtp[0];
ccx qtp[0],qta[0],qta[1];
x qtp[0];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
x qtp[1];
ccx qtp[1],qta[1],qta[0];
x qtp[1];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
ccx qtp[2],qta[0],qta[1];
reset qta[0];
x qtp[3];
ccx qtp[3],qta[1],qta[0];
x qtp[3];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[0],qta[0],qg[0];
ccx qia[0],qta[0],qg[1];
ccx qia[0],qta[0],qg[2];
ccx qia[0],qta[0],qg[3];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
x qtp[0];
ccx qtp[0],qta[0],qta[1];
x qtp[0];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
x qtp[1];
ccx qtp[1],qta[1],qta[0];
x qtp[1];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
ccx qtp[2],qta[0],qta[1];
reset qta[0];
ccx qtp[3],qta[1],qta[0];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
x qtp[0];
ccx qtp[0],qta[0],qta[1];
x qtp[0];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
ccx qtp[1],qta[1],qta[0];
reset qta[1];
x qtp[2];
ccx qtp[2],qta[0],qta[1];
x qtp[2];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
x qtp[3];
ccx qtp[3],qta[1],qta[0];
x qtp[3];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
x qtp[0];
ccx qtp[0],qta[0],qta[1];
x qtp[0];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
ccx qtp[1],qta[1],qta[0];
reset qta[1];
x qtp[2];
ccx qtp[2],qta[0],qta[1];
x qtp[2];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
ccx qtp[3],qta[1],qta[0];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[0],qta[0],qg[0];
ccx qia[0],qta[0],qg[1];
ccx qia[0],qta[0],qg[2];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
x qtp[0];
ccx qtp[0],qta[0],qta[1];
x qtp[0];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
ccx qtp[1],qta[1],qta[0];
reset qta[1];
ccx qtp[2],qta[0],qta[1];
reset qta[0];
x qtp[3];
ccx qtp[3],qta[1],qta[0];
x qtp[3];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[0],qta[0],qg[3];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
x qtp[0];
ccx qtp[0],qta[0],qta[1];
x qtp[0];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
ccx qtp[1],qta[1],qta[0];
reset qta[1];
ccx qtp[2],qta[0],qta[1];
reset qta[0];
ccx qtp[3],qta[1],qta[0];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
ccx qtp[0],qta[0],qta[1];
reset qta[0];
x qtp[1];
ccx qtp[1],qta[1],qta[0];
x qtp[1];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
x qtp[2];
ccx qtp[2],qta[0],qta[1];
x qtp[2];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
x qtp[3];
ccx qtp[3],qta[1],qta[0];
x qtp[3];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[0],qta[0],qg[3];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
ccx qtp[0],qta[0],qta[1];
reset qta[0];
x qtp[1];
ccx qtp[1],qta[1],qta[0];
x qtp[1];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
x qtp[2];
ccx qtp[2],qta[0],qta[1];
x qtp[2];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
ccx qtp[3],qta[1],qta[0];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[0],qta[0],qg[0];
ccx qia[0],qta[0],qg[2];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
ccx qtp[0],qta[0],qta[1];
reset qta[0];
x qtp[1];
ccx qtp[1],qta[1],qta[0];
x qtp[1];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
ccx qtp[2],qta[0],qta[1];
reset qta[0];
x qtp[3];
ccx qtp[3],qta[1],qta[0];
x qtp[3];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[0],qta[0],qg[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
ccx qtp[0],qta[0],qta[1];
reset qta[0];
x qtp[1];
ccx qtp[1],qta[1],qta[0];
x qtp[1];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
ccx qtp[2],qta[0],qta[1];
reset qta[0];
ccx qtp[3],qta[1],qta[0];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
ccx qtp[0],qta[0],qta[1];
reset qta[0];
ccx qtp[1],qta[1],qta[0];
reset qta[1];
x qtp[2];
ccx qtp[2],qta[0],qta[1];
x qtp[2];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
x qtp[3];
ccx qtp[3],qta[1],qta[0];
x qtp[3];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
ccx qtp[0],qta[0],qta[1];
reset qta[0];
ccx qtp[1],qta[1],qta[0];
reset qta[1];
x qtp[2];
ccx qtp[2],qta[0],qta[1];
x qtp[2];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[0];
ccx qtp[3],qta[1],qta[0];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
ccx qtp[0],qta[0],qta[1];
reset qta[0];
ccx qtp[1],qta[1],qta[0];
reset qta[1];
ccx qtp[2],qta[0],qta[1];
reset qta[0];
x qtp[3];
ccx qtp[3],qta[1],qta[0];
x qtp[3];
barrier qtp[0],qtp[1],qtp[2],qtp[3];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
ccx qtp[0],qta[0],qta[1];
reset qta[0];
ccx qtp[1],qta[1],qta[0];
reset qta[1];
ccx qtp[2],qta[0],qta[1];
reset qta[0];
ccx qtp[3],qta[1],qta[0];
reset qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qta[0];
reset qta[1];
x qta[0];
reset qia[0];
reset qia[1];
x qia[0];
cx qtp[0],qia[1];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qia[1],qg[0],tv1[0];
ccx qia[1],qg[1],tv1[1];
ccx qia[1],qg[2],tv1[2];
ccx qia[1],qg[3],tv1[3];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
x qtp[0];
cx qtp[0],qta[1];
x qtp[0];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx qta[1],qg[0],tv2[0];
ccx qta[1],qg[1],tv2[1];
ccx qta[1],qg[2],tv2[2];
ccx qta[1],qg[3],tv2[3];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
reset qia[0];
reset qia[1];
reset qta[0];
reset qta[1];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
cx tv2[0],tv1[0];
cx tv2[1],tv1[1];
cx tv2[2],tv1[2];
cx tv2[3],tv1[3];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
cx tv2[1],qia[0];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx tv1[0],tv2[0],qia[0];
cx tv2[2],tv2[1];
cx tv2[3],tv2[2];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
x tv1[0];
ccx tv1[0],qta[0],qia[0];
x tv1[0];
x tv1[1];
ccx qia[0],tv1[1],tv2[1];
x tv1[1];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
x tv1[2];
ccx tv2[1],tv1[2],tv2[2];
x tv1[2];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
cx tv2[3],qia[1];
x tv1[3];
ccx tv2[2],tv1[3],qia[1];
x tv1[3];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
x tv1[2];
ccx tv2[1],tv1[2],tv2[2];
x tv1[2];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
x tv1[1];
ccx qia[0],tv1[1],tv2[1];
x tv1[1];
x tv1[0];
ccx tv1[0],qta[0],qia[0];
x tv1[0];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
ccx tv1[0],tv2[0],qia[0];
cx tv2[3],tv2[2];
cx tv2[2],tv2[1];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
cx tv2[1],qia[0];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
cx tv2[0],tv1[0];
cx tv2[1],tv1[1];
cx tv2[2],tv1[2];
cx tv2[3],tv1[3];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
barrier tv1[0],tv1[1],tv1[2],tv1[3],tv2[0],tv2[1],tv2[2],tv2[3],qg[0],qg[1],qg[2],qg[3],qp[0],qp[1],qp[2],qp[3],qtp[0],qtp[1],qtp[2],qtp[3],qia[0],qia[1],qta[0],qta[1];
measure tv1[0] -> meas[0];
measure tv1[1] -> meas[1];
measure tv1[2] -> meas[2];
measure tv1[3] -> meas[3];
measure tv2[0] -> meas[4];
measure tv2[1] -> meas[5];
measure tv2[2] -> meas[6];
measure tv2[3] -> meas[7];
measure qg[0] -> meas[8];
measure qg[1] -> meas[9];
measure qg[2] -> meas[10];
measure qg[3] -> meas[11];
measure qp[0] -> meas[12];
measure qp[1] -> meas[13];
measure qp[2] -> meas[14];
measure qp[3] -> meas[15];
measure qtp[0] -> meas[16];
measure qtp[1] -> meas[17];
measure qtp[2] -> meas[18];
measure qtp[3] -> meas[19];
measure qia[0] -> meas[20];
measure qia[1] -> meas[21];
measure qta[0] -> meas[22];
measure qta[1] -> meas[23];


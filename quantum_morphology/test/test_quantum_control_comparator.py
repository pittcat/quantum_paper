#!/usr/bin/env python
# -*- coding=utf-8 -*-

import pytest
from qiskit import Aer, transpile

import qiskit
from qiskit import QuantumRegister, QuantumCircuit
from quantum_comparator import control_quantum_comparator

from qiskit import IBMQ

num1, num2, control, assist = 4, 4, 1, 2
shot_times = 8192

qn1 = QuantumRegister(num1, "qn1")
qn2 = QuantumRegister(num2, "qn2")
control_q = QuantumRegister(control, "cq")
qas = QuantumRegister(assist, "qas")  # qas[1] shows result


def res_check(state, a, b):
    # state 0(a1a0 >= b1b0)
    if state == 0:
        return a >= b
    else:
        return a < b


@pytest.fixture(scope="session")
def circ_lst_init():

    circ = QuantumCircuit(qn1, qn2, control_q, qas)
    num_info = num1 + num2
    digits = num1
    num1_indexes = list(range(num1))
    num2_indexes = list(range(num1, num_info))
    control_idx = num_info
    assist_indexes = list(range(num_info + 1, num_info + 4))

    #  assignment value
    circ.h(qn1[0:num1])
    circ.x(qn2[2:num1])

    control_quantum_comparator(
        circ, digits, num1_indexes, num2_indexes, control_idx, assist_indexes
    )

    circ.draw(output="mpl", filename="control_comparator.png", scale=0.5, fold=60)
    circ.measure_all()

    # cloud backend
    # provider = IBMQ.load_account()
    # print(provider)
    # backend = provider.get_backend("simulator_mps")
    # result_ideal = qiskit.execute(circ, backend, shots=shot_times).result()
    # counts = result_ideal.get_counts(0)

    # run circuit on local simulator
    # simulator = Aer.get_backend("aer_simulator")
    # circ = transpile(circ, simulator)
    # result = simulator.run(circ, shots=160).result()
    # counts = result.get_counts(circ)

    #  print('Counts(ideal):', counts)
    # counts_lst = [i[::-1] for i in counts.keys()]
    # return counts_lst


def test_res_check(circ_lst_init):
    # count_num = 0
    # for num_string in circ_lst_init:
    #     num1_value = int(num_string[0:num1][::-1], 2)
    #     num2_value = int(num_string[num1 : num1 + num2][::-1], 2)
    #     state = int(num_string[-1])
    #     print("num1_string:", num_string[0:num1][::-1])
    #     print("num2_string:", num_string[num1 : num1 + num2][::-1])
    #     print("num1:", num1_value, "num2:", num2_value)
    #     print("state:", state)
    #     print("\n")
    #     assert res_check(state, num1_value, num2_value)
    #     count_num += 1
    assert 1

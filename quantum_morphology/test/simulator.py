#!/usr/bin/env python
# -*- coding=utf-8 -*-

import qiskit
from qiskit import IBMQ
from qiskit.providers.aer import AerSimulator

# Generate 3-qubit GHZ state
circ = qiskit.QuantumCircuit(3)
circ.h(0)
circ.cx(0, 1)
circ.cx(1, 2)
circ.measure_all()

# Construct an ideal simulator
#  IBMQ.save_account('74fa6b12c518f87a1435c5c351e2e9315fe9202d96933df52fdb850fb3ad991230d43480d7e85fbb25d0fb763a74a25a19c96cbaab62b240f23337bdd3dbe9f3')
provider = IBMQ.load_account()
print(provider)
backend = provider.get_backend('simulator_mps')
result_ideal = qiskit.execute(circ, backend,shots=10).result()
counts_ideal = result_ideal.get_counts(0)
print('Counts(ideal):', counts_ideal)

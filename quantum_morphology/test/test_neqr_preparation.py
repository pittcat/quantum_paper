#!/usr/bin/env python
# -*- coding=utf-8 -*-

import pytest
import itertools

from neqr_preparation import (
    gen_quantum_circuit,
    pos_correspondence,
    fix_gray_info,
    reset_origin_auxiliary,
)
from qiskit.providers.aer import AerSimulator
import qiskit
from qiskit import IBMQ

# [[ 7 12 15  0]
#  [ 0 14  1  0]

#  [ 1 10  4  0]
#  [ 0  0  0  0]]
template_neqr = [
    [
        "0111000",
        "1100001",
        "1111010",
        "0000011",
        "0000100",
        "1110101",
        "0001110",
        "0000111",
    ],
    [
        "0001000",
        "1010001",
        "0100010",
        "0000011",
        "0000100",
        "0000101",
        "0000110",
        "0000111",
    ],
]

gray, pos, template = 4, 4, 3
info_sum = sum([gray * 2, pos, template * 2])
original_pos = "0101"


@pytest.fixture
def circ_init():
    print(info_sum)
    circ = gen_quantum_circuit(gray, pos, template)
    circ.h(range(gray * 2, info_sum))  # init position and template position qbits

    circ.x(
        [info_sum, info_sum + 2]
    )  # init position auxiliary and template position auxiliary qbits
    circ = pos_correspondence(
        circ,
        original_pos,
        list(range(gray * 2, gray * 2 + pos)),
        list(range(info_sum, info_sum + 2)),
    )  # fix original position info to first position auxiliary qbit
    circ.barrier()
    original_pos_idx = info_sum
    template_pos_idx = info_sum + 3

    # up template matrix
    for i in template_neqr[0]:
        cur_pos = i[-template:]
        print(cur_pos)
        circ = pos_correspondence(
            circ,
            cur_pos,
            list(range(gray * 2 + pos, info_sum - template)),
            list(range(info_sum + 2, info_sum + 4)),
        )  # fix template position info to first template position auxiliary qbit
        circ.barrier()
        fix_gray_info(
            circ, i[:gray], list(range(gray)), original_pos_idx, template_pos_idx
        )  # fix gray info to quantum circuit
        circ.barrier()
        reset_origin_auxiliary(circ, [template_pos_idx, template_pos_idx - 1])

        circ.barrier()
    # down template matrix
    for i in template_neqr[1]:
        cur_pos = i[-template:]
        print(cur_pos)
        circ = pos_correspondence(
            circ,
            cur_pos,
            list(range(gray * 2 + pos + template, info_sum)),
            list(range(info_sum + 2, info_sum + 4)),
        )  # fix template position info to first template position auxiliary qbit
        circ.barrier()
        fix_gray_info(
            circ,
            i[:gray],
            list(range(gray, gray * 2)),
            original_pos_idx,
            template_pos_idx,
        )  # fix gray info to quantum circuit
        circ.barrier()
        reset_origin_auxiliary(circ, [template_pos_idx, template_pos_idx - 1])

        circ.barrier()

    reset_origin_auxiliary(circ, [original_pos_idx + 1, original_pos_idx])

    circ.barrier()
    circ.draw(output="mpl", filename="ineqr_preparation.png", scale=0.5, fold=50)

    circ.measure_all()

    #  local simulator
    # aersim = AerSimulator()
    # result_ideal = qiskit.execute(circ, aersim, shots=1200).result()
    # counts = result_ideal.get_counts(0)

    # cloud simulator
    provider = IBMQ.load_account()
    print(provider)
    backend = provider.get_backend("simulator_mps")
    result_ideal = qiskit.execute(circ, backend, shots=8000).result()
    counts = result_ideal.get_counts(0)

    counts_lst = [i[::-1] for i in counts.keys()]
    print(counts_lst)
    print(len(counts_lst))
    return counts_lst


def res_extract(counts_data):
    """
    function: extract group template info from counts_lst
    """
    filter_lst = up_lst = down_lst = []

    for _ in counts_data:
        if _[gray * 2 : gray * 2 + pos] == original_pos:
            filter_lst.append(_)
    down_lst = [
        _[gray : gray * 2] + _[gray * 2 + pos + 3 : gray * 2 + pos + 6 :]
        for _ in filter_lst
        if _[gray * 2 + pos : gray * 2 + pos + 3] == "000"
    ]
    up_lst = [
        _[0:gray] + _[gray * 2 + pos : gray * 2 + pos + 3]
        for _ in filter_lst
        if _[gray * 2 + pos + 3 : gray * 2 + pos + 6] == "000"
    ]

    return up_lst + down_lst


def test_ineqr_preparation(circ_init):
    with open("neqr_preparation_counts_lst.txt", "w") as outfile:
        for i in circ_init:
            print(i, file=outfile)
    template_merge_neqr = itertools.chain.from_iterable(template_neqr)

    res_lst = res_extract(circ_init)
    assert sorted(res_lst) == sorted(template_merge_neqr)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
import numpy as np
import os
import cv2
import common_func_sets


@pytest.fixture
def common_func_sets_init():
    file_dir = os.path.abspath(
        os.path.join(os.path.dirname(__file__), '..', 'image'))
    file_name = file_dir + "/in-4.png"
    image_array = cv2.imread(file_name, cv2.IMREAD_GRAYSCALE)
    return [file_name, image_array]


def test_get_image_matrix(common_func_sets_init):
    image_matrix = common_func_sets.get_image_matrix('in-4.png', 'image')
    assert (common_func_sets_init[1] == image_matrix).all()


def test_expand_image_matrix(common_func_sets_init):
    matrix = np.pad(common_func_sets_init[1], (1, 1), 'edge')
    expanded_matrix = common_func_sets.expand_image_matrix('in-4.png', 'image')
    (matrix == expanded_matrix).all()


def test_get_image_block(common_func_sets_init):
    matrix = np.pad(common_func_sets_init[1], (1, 1), 'edge')
    #  print(next(common_func_sets.get_image_block(3, matrix)))
    test_matrix = np.array([[7, 7, 12], [7, 7, 12], [0, 0, 14]])
    matrix = next(common_func_sets.get_image_block(3, matrix))
    print("tests:", test_matrix)
    print(type(test_matrix))
    print("matrix:", matrix)
    print(type(matrix))
    assert (test_matrix == matrix).all()


def test_gen_position(common_func_sets_init):
    matrix = common_func_sets_init[1]
    test_matrix = [
        '0000', '0001', '0010', '0011', '0100', '0101', '0110', '0111', '1000',
        '1001', '1010', '1011', '1100', '1101', '1110', '1111'
    ]
    assert common_func_sets.gen_position_info(matrix) == test_matrix


def test_array2neqrstring():
    matrix = np.array([[7, 7, 12], [7, 7, 12], [0, 0, 14]])
    matrix = np.pad(matrix, (0, 1), 'constant', constant_values=0)
    neqr_lst = common_func_sets.array2neqrstring(matrix, 4)
    test_neqr_lst = [
        '01110000', '01110001', '11000010', '00000011', '01110100', '01110101',
        '11000110', '00000111', '00001000', '00001001', '11101010', '00001011',
        '00001100', '00001101', '00001110', '00001111'
    ]
    assert neqr_lst == test_neqr_lst

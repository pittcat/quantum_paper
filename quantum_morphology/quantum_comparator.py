#!/usr/bin/env python
# -*- coding: utf-8 -*-


def quantum_comparator(circ, digits, num1_idx, num2_idx, assist_idx):
    """
    :function: add comparator for quantum circuit
    :circ: quantum circuit
    :digits: the number of occupied bits
    :num1_idx: list that constructed by num1 indexes(low to high)
    :num2_idx: list that constructed by num2 indexes(low to high)
    :assist_idx: list(the len of list is 2) that constructed by qbits
    assist index(comparator state == assist_idx[1].num1 is up,num2 is low.
    num1>=num2,assist_idx[1]=0;num1<num2,assist_idx[1]=1)
    """
    # part 1
    for i in range(digits):
        circ.cx(num2_idx[i], num1_idx[i])
    circ.barrier()
    # part 2
    circ.cx(num2_idx[1], assist_idx[0])
    circ.barrier()
    # part 3
    circ.ccx(num1_idx[0], num2_idx[0], assist_idx[0])
    for i in range(1, digits - 1):
        circ.cx(num2_idx[i + 1], num2_idx[i])
    circ.barrier()
    # part 4
    circ.x(num1_idx[0])
    # circ.ccx(num1_idx[0], assist_idx[2], assist_idx[0])
    circ.x(num1_idx[0])
    if digits > 2:
        circ.x(num1_idx[1])
        circ.ccx(assist_idx[0], num1_idx[1], num2_idx[1])
        circ.x(num1_idx[1])
        circ.barrier()
    # part 5
    for i in range(1, digits - 2):
        circ.x(num1_idx[i + 1])
        circ.ccx(num2_idx[i], num1_idx[i + 1], num2_idx[i + 1])
        circ.x(num1_idx[i + 1])
    circ.barrier()

    # assign assist2(c1) value
    circ.cx(num2_idx[-1], assist_idx[1])
    circ.x(num1_idx[-1])
    if digits > 2:
        circ.ccx(num2_idx[-2], num1_idx[-1], assist_idx[1])
    else:
        circ.ccx(assist_idx[0], num1_idx[-1], assist_idx[1])
    circ.x(num1_idx[-1])
    circ.barrier()

    # symmetry progress
    # part 5
    for i in range(digits - 3, 0, -1):
        circ.x(num1_idx[i + 1])
        circ.ccx(num2_idx[i], num1_idx[i + 1], num2_idx[i + 1])
        circ.x(num1_idx[i + 1])
    circ.barrier()
    # part 4
    if digits > 2:
        circ.x(num1_idx[1])
        circ.ccx(assist_idx[0], num1_idx[1], num2_idx[1])
        circ.x(num1_idx[1])
    circ.x(num1_idx[0])
    # circ.ccx(num1_idx[0], assist_idx[2], assist_idx[0])
    circ.x(num1_idx[0])
    circ.barrier()
    # part 3
    circ.ccx(num1_idx[0], num2_idx[0], assist_idx[0])
    for i in range(digits - 1, 1, -1):
        circ.cx(num2_idx[i], num2_idx[i - 1])
    circ.barrier()
    # part 2
    circ.cx(num2_idx[1], assist_idx[0])
    circ.barrier()
    # part 1
    for i in range(digits):
        circ.cx(num2_idx[i], num1_idx[i])
    circ.barrier()

    # restore value
    circ.barrier()
    #  circ.x(num1_idx[-1])
    return circ


def control_quantum_comparator(
    circ, digits, num1_idx, num2_idx, control_idx, assist_idx
):
    """
    :function: add comparator for quantum circuit
    :circ: quantum circuit
    :digits: the number of occupied bits
    :num1_idx: list that constructed by num1 indexes(low to high)
    :num2_idx: list that constructed by num2 indexes(low to high)
    :control_idx: control qbit idx
    :assist_idx: list(the len of list is 2) that constructed by qbits
    assist index(comparator state == assist_idx[1]. num1 is up,num2 is low.
    num1>=num2,assist_idx[1]=0;num1<num2,assist_idx[1]=1).
    """
    # part 1
    for i in range(digits):
        circ.cx(num2_idx[i], num1_idx[i])
    circ.barrier()
    # part 2
    circ.cx(num2_idx[1], assist_idx[0])
    circ.barrier()
    # part 3
    circ.ccx(num1_idx[0], num2_idx[0], assist_idx[0])
    for i in range(1, digits - 1):
        circ.cx(num2_idx[i + 1], num2_idx[i])
    circ.barrier()
    # part 4
    circ.x(num1_idx[0])
    # circ.ccx(num1_idx[0], assist_idx[2], assist_idx[0])
    circ.x(num1_idx[0])
    if digits > 2:
        circ.x(num1_idx[1])
        circ.ccx(assist_idx[0], num1_idx[1], num2_idx[1])
        circ.x(num1_idx[1])
        circ.barrier()
    # part 5
    for i in range(1, digits - 2):
        circ.x(num1_idx[i + 1])
        circ.ccx(num2_idx[i], num1_idx[i + 1], num2_idx[i + 1])
        circ.x(num1_idx[i + 1])
    circ.barrier()

    # assign assist2(c1) value
    circ.ccx(control_idx, num2_idx[-1], assist_idx[1])
    circ.x(num1_idx[-1])
    if digits > 2:
        circ.mcx([control_idx, num2_idx[-2], num1_idx[-1]], assist_idx[1])
    else:
        circ.mcx([control_idx, assist_idx[0], num1_idx[-1]], assist_idx[1])
    circ.x(num1_idx[-1])
    circ.barrier()

    # symmetry progress
    # part 5
    for i in range(digits - 3, 0, -1):
        circ.x(num1_idx[i + 1])
        circ.ccx(num2_idx[i], num1_idx[i + 1], num2_idx[i + 1])
        circ.x(num1_idx[i + 1])
    circ.barrier()
    # part 4
    if digits > 2:
        circ.x(num1_idx[1])
        circ.ccx(assist_idx[0], num1_idx[1], num2_idx[1])
        circ.x(num1_idx[1])
    circ.x(num1_idx[0])
    # circ.ccx(num1_idx[0], assist_idx[2], assist_idx[0])
    circ.x(num1_idx[0])
    circ.barrier()
    # part 3
    circ.ccx(num1_idx[0], num2_idx[0], assist_idx[0])
    for i in range(digits - 1, 1, -1):
        circ.cx(num2_idx[i], num2_idx[i - 1])
    circ.barrier()
    # part 2
    circ.cx(num2_idx[1], assist_idx[0])
    circ.barrier()
    # part 1
    for i in range(digits):
        circ.cx(num2_idx[i], num1_idx[i])
    circ.barrier()

    # restore value
    circ.barrier()
    #  circ.x(num1_idx[-1])
    return circ

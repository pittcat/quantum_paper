#!/usr/bin/env python
# -*- coding: utf-8 -*-

from qiskit.providers.aer import QasmSimulator
from qiskit import QuantumRegister, QuantumCircuit

from qiskit import transpile

num1, num2, res, assist = 2, 2, 1, 2
qn1 = QuantumRegister(num1, 'qn1')
qn2 = QuantumRegister(num2, 'qn2')
res = QuantumRegister(res, 'res')
qas = QuantumRegister(assist, 'qas')
circ = QuantumCircuit(qn1, qn2, qas, res)

#  assignment value
circ.h(qn1[0:2])
circ.h(qn2[0:2])

circ.cx(qn2[0], qn1[0])
circ.cx(qn2[1], qn1[1])
circ.cx(qn2[1], qas[0])
circ.barrier()
circ.ccx(qn1[0], qn2[0], qas[0])
circ.cx(qn2[1], qas[1])
circ.barrier()

circ.x(qn1[0])
circ.ccx(res, qn1[0], qas[0])
circ.barrier()

circ.x(qn1[1])
circ.ccx(qas[0], qn1[1], qas[1])
circ.barrier()

circ.x(qn1[0])
circ.ccx(res, qn1[0], qas[0])
circ.barrier()

circ.ccx(qn1[0], qn2[0], qas[0])

circ.barrier()
circ.cx(qn2[0], qn1[0])
circ.cx(qn2[1], qn1[1])
circ.cx(qn2[1], qas[0])

circ.barrier()
circ.x(qn1[1])

circ.draw(output='mpl', filename='circuit-debug.png', scale=0.5, fold=50)
circ.measure_all()

backend = QasmSimulator()
circ_compiled = transpile(circ, backend)
job_sim = backend.run(circ, shots=80)
result_sim = job_sim.result()
counts = result_sim.get_counts(circ_compiled)
counts_lst = [i[::-1] for i in counts.keys()]
print(counts_lst)
print(len(counts_lst))

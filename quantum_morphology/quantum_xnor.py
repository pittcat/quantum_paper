#!/usr/bin/env python
# -*- coding=utf-8 -*-

from qiskit import QuantumCircuit, QuantumRegister
from qiskit import Aer, transpile
from common_func_sets import counts2lst, encrypt, xnor


c_n = 3
c1 = QuantumRegister(c_n, "c1")
c2 = QuantumRegister(c_n, "c2")
assit_q = QuantumRegister(3, "assit")
circ = QuantumCircuit(c1, c2, assit_q)


circ.h(c1)
circ.h(c2)
circ.x(assit_q[1])

circ.barrier()
circ = xnor(
    circ, list(range(c_n)), list(range(c_n, c_n * 2)), list(range(c_n * 2, c_n * 2 + 3))
)


circ.measure_all()

# # run circuit on local simulator
# simulator = Aer.get_backend("aer_simulator")
# circ = transpile(circ, simulator)
# result = simulator.run(circ, shots=30).result()
# counts = result.get_counts(circ)

# # convert counts to list
# counts_lst = counts2lst(counts)
# for _ in counts_lst:
#     print(encrypt(_, 2))

circ.draw(output="mpl", filename="xnor.png", scale=0.5, fold=50)

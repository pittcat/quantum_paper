import numpy as np
import cv2
import os
import png


def image_random_gen(scope, dimension):
    #  Generate the input image
    in_image_matrix = np.random.randint(
        scope, size=(dimension, dimension)).astype('u1')
    __import__('pprint').pprint(in_image_matrix)
    in_file = os.path.dirname(os.path.realpath(
        __file__)) + '/' + 'in' + '-' + str(dimension) + '.png'
    png.from_array(in_image_matrix, 'L').save(in_file)




def main():
    dimensions = [pow(2, x) for x in range(1, 4)]
    scope = 16
    for dimension in dimensions:
        image_random_gen(scope, dimension)


if __name__ == "__main__":
    main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import cv2
import qiskit
from qiskit import IBMQ
import numpy as np
from math import log2


def get_image_matrix(file, dirname):
    """
    :function: get_image_array get image matrix
    :returns: image matrix
    """
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/" + dirname + "/" + file

    image_array = cv2.imread(file_path, cv2.IMREAD_GRAYSCALE)
    return image_array


def expand_image_matrix(file, dirname):
    """
    :function: expand_image_matrix generate an extended array for morphology
    :return: padded array
    """
    padded_array = np.pad(get_image_matrix(file, dirname), (1, 1), "edge")
    return padded_array


def get_image_block(t_size, image_matrix):
    """
    :function: generate all template image array
    :t_size: template block side size
    :image_matrix: image matrix
    :return: generator object of template image array
    """
    x, y = image_matrix.shape
    step = t_size - 1
    for i in range(x - step):
        for j in range(y - step):
            yield image_matrix[i : i + t_size, j : j + t_size]


def gen_position_info(image_matrix):
    """
    :function: generate infomation of pixel position
    :image_matrix: n*n image matrix(n is the power of 2)
    :return: a list that contains position infomation
    """
    pos_lst = []
    shape_size = image_matrix.shape[0]
    pos_size = int(log2(shape_size)) * 2
    for _ in range(pow(shape_size, 2)):
        pos_lst.append(bin(_)[2:].zfill(pos_size))

    return pos_lst


def array2neqrstring(block_matrix, gray_length, pos_size=2):
    """
    :function: convert template block matrix element to NEQR string
    :block_matrix: template block matrix
    :gray_length: bit of pixel gray info
    :pos_size: bit of pixel position info
    :return: NEQR string
    """
    quan_states = []
    shape_size = block_matrix.shape[0]
    for Y in range(shape_size):
        for X in range(shape_size):
            expr_y, expr_x, gray_v = (
                bin(Y)[2:].zfill(pos_size),
                bin(X)[2:].zfill(pos_size),
                bin(block_matrix[Y][X])[2:].zfill(gray_length),
            )
            quan_states.append(gray_v + expr_y + expr_x)  #
    return quan_states


def half_template2neqrstring(template_matrix, gray_length, pos_size=2):
    """
    :function: convert half template block matrix element to NEQR string
    :template_matrix:half template block matrix
    :gray_length: bit of pixel gray info
    :pos_size: bit of pixel position info
    :return: NEQR string list
    """
    quan_states = []
    row, col = template_matrix.shape[0], template_matrix.shape[1]
    for Y in range(row):
        for X in range(col):
            expr_y, expr_x, gray_v = (
                bin(Y)[2:].zfill(pos_size - 1),
                bin(X)[2:].zfill(pos_size),
                bin(template_matrix[Y][X])[2:].zfill(gray_length),
            )
            quan_states.append(gray_v + expr_y + expr_x)  #
    return quan_states


def get_quantum_circuit_out(circuit, shot_times, simulator="simulator_mps"):
    """
    :function: get quantum circuit output and convert output to a list
    :circuit: quantum circuit
    :simulator: simulator name
    :shots_times: quantum circuit running times
    """
    provider = IBMQ.load_account()
    print(provider)
    backend = provider.get_backend(simulator)
    result_ideal = qiskit.execute(circuit, backend, shots=shot_times).result()
    counts = result_ideal.get_counts(0)
    return counts


def counts2lst(counts):
    """
    :function: convert counts(dict) to a string lst
    :return: counts list(from top to bottom)
    """
    counts_lst = [i[::-1] for i in counts.keys()]
    return counts_lst


def copy_gray(circ, ass_idx, src_lst, dst_lst, L):
    """
    :circ: quantum circuit
    :assist_idx: fixed idx that assists assignment
    :src_lst: source list that stores gray info
    :dst_lst: target list that stores gray info
    :L: the length of list
    """
    for _ in range(L):
        circ.ccx(ass_idx, src_lst[_], dst_lst[_])
    return circ


def xnor(circ, src, dst, assit_lst):
    """
    :circ: quantum circuit
    :src: source idx lst
    :dst: target idx lst
    :assit_lst: assit qbits lst(the size is 3, assit_lst[0] stores the temp
            state and the initial situation of assit_lst[1] is 1 )
    """
    L = len(src)
    for i in range(L):
        circ.cx(src[i], assit_lst[0])
        circ.cx(dst[i], assit_lst[0])
        circ.x(assit_lst[0])
        circ.barrier()
        if i % 2 == 0:
            circ.ccx(assit_lst[0], assit_lst[1], assit_lst[2])
            circ.reset(assit_lst[0])
            circ.reset(assit_lst[1])
            circ.barrier()
        else:
            circ.ccx(assit_lst[0], assit_lst[2], assit_lst[1])
            circ.reset(assit_lst[0])
            circ.reset(assit_lst[2])
            circ.barrier()

    return circ


def encrypt(string, length):
    """
    :function: insert a space for every length characters in the string
    :string: string
    :length: character length
    """
    return " ".join(string[i : i + length] for i in range(0, len(string), length))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from common_func_sets import (get_image_matrix, expand_image_matrix,
                              get_image_block, half_template2neqrstring,
                              gen_position_info)
import numpy as np

def main():
    init_matrix = get_image_matrix('in-4.png', 'image')
    print(init_matrix)
    print(type(init_matrix))
    pos_lst = gen_position_info(init_matrix)
    print("pos_lst:",pos_lst)
    padded_matrix = expand_image_matrix('in-4.png', 'image')
    print(padded_matrix)
    for i, j in zip(get_image_block(3, padded_matrix), pos_lst):
        i = np.pad(i, (0, 1), 'constant',
                   constant_values=0)  # template block for max value
        print("template block_matrix:")
        print(i)
        #  np.pad(i, (0, 1), 'constant',
        #  constant_values=255)  # template block for min value
        print(half_template2neqrstring(i[0:2], 4))
        print(half_template2neqrstring(i[2:], 4))
        print(j)
    #  f = open("counts_lst.txt")
    #  lines = f.readlines()[::-1]
    #  print(lines)
    return 0


if __name__ == "__main__":
    main()

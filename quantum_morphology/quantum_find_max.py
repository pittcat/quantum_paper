#!/usr/bin/env python
# -*- coding: utf-8 -*-

from quantum_comparator import control_quantum_comparator
from neqr_preparation import (
    gen_quantum_circuit,
    pos_correspondence,
    fix_gray_info,
    reset_origin_auxiliary,
)


from qiskit.circuit.quantumcircuit import QuantumCircuit, QuantumRegister
from common_func_sets import get_quantum_circuit_out, counts2lst, copy_gray, xnor

gray, pos, template = 4, 4, 3

image_info = gray * 2 + pos
info_sum = sum([gray * 2, pos, template * 2])
original_pos_idx = info_sum
template_pos_idx = info_sum + 3

# image preparation part

# [[ 7 12 15  0]
#  [ 0 14  1  0]
#  [ 1 10  4  0]
#  [ 0  0  0  0]]
# template_neqr = [
#     [
#         "0111000",
#         "1100001",
#         "1111010",
#         "0000011",
#         "0000100",
#         "1110101",
#         "0001110",
#         "0000111",
#     ],
#     [
#         "0001000",
#         "1010001",
#         "0100010",
#         "0000011",
#         "0000100",
#         "0000101",
#         "0000110",
#         "0000111",
#     ],
# ]

# original_pos = "0101"

# print(info_sum)
# circ = gen_quantum_circuit(gray, pos, template)
# circ.h(range(gray * 2, info_sum))  # init position and template position qbits

# circ.x(
#     [info_sum, info_sum + 2]
# )  # init position auxiliary and template position auxiliary qbits
# circ = pos_correspondence(
#     circ,
#     original_pos,
#     list(range(gray * 2, image_info)),
#     list(range(info_sum, info_sum + 2)),
# )  # fix original position info to first position auxiliary qbit
# circ.barrier()

# # up template matrix
# for i in template_neqr[0]:
#     cur_pos = i[-template:]
#     print(cur_pos)
#     circ = pos_correspondence(
#         circ,
#         cur_pos,
#         list(range(gray * 2 + pos, info_sum - template)),
#         list(range(info_sum + 2, info_sum + 4)),
#     )  # fix template position info to first template position auxiliary qbit
#     circ.barrier()
#     fix_gray_info(
#         circ, i[:gray], list(range(gray)), original_pos_idx, template_pos_idx
#     )  # fix gray info to quantum circuit
#     circ.barrier()
#     reset_origin_auxiliary(circ, [template_pos_idx, template_pos_idx - 1])

#     circ.barrier()
# # down template matrix
# for i in template_neqr[1]:
#     cur_pos = i[-template:]
#     print(cur_pos)
#     circ = pos_correspondence(
#         circ,
#         cur_pos,
#         list(range(gray * 2 + pos + template, info_sum)),
#         list(range(info_sum + 2, info_sum + 4)),
#     )  # fix template position info to first template position auxiliary qbit
#     circ.barrier()
#     fix_gray_info(
#         circ,
#         i[:gray],
#         list(range(gray, gray * 2)),
#         original_pos_idx,
#         template_pos_idx,
#     )  # fix gray info to quantum circuit
#     circ.barrier()
#     reset_origin_auxiliary(circ, [template_pos_idx, template_pos_idx - 1])

#     circ.barrier()

# reset_origin_auxiliary(circ, [original_pos_idx + 1, original_pos_idx])
# circ.draw(output="mpl", filename="ineqr_preparation.png", scale=0.5, fold=50)

# attatch neqr_preparation circ part to the final circ part


qbit_name_lst = [
    "gray1",
    "gray2",
    "original_pos",
    "temp_up",
    "temp_down",
    "assist_qbit",
]
qbit_count_lst = [gray, gray, pos, template, template, 4]
qbit_lst = [QuantumRegister(i, j) for i, j in list(zip(qbit_count_lst, qbit_name_lst))]

final_circ = QuantumCircuit(*qbit_lst)
# final_circ = xnor(
#     final_circ,
#     list(range(info_sum - 6, info_sum - 3)),
#     list(range(info_sum - 3, info_sum)),
#     [info_sum + 1, info_sum, info_sum + 3],
# )
# final_circ.barrier()

# final_circ = control_quantum_comparator(
#     final_circ,
#     gray,
#     list(range(gray)),
#     list(range(gray, 2 * gray)),
#     info_sum + 3,
#     [info_sum, info_sum + 1],
# )

# swap value
for i in range(gray):
    final_circ.cswap(info_sum+1,i,i+gray)


final_circ.barrier()
reset_origin_auxiliary(final_circ, [template_pos_idx, template_pos_idx - 1])
final_circ.barrier()
reset_origin_auxiliary(final_circ, [original_pos_idx + 1, original_pos_idx])
final_circ.barrier()


final_circ = xnor(
    final_circ,
    [image_info + 1, image_info + 2],
    [image_info + 4, image_info + 5],
    [info_sum + 1, info_sum, info_sum + 3],
)
final_circ.reset(list(range(info_sum + 1, info_sum + 4)))

final_circ.barrier()
final_circ.ccx(image_info, image_info + 3, template_pos_idx)
final_circ.ccx(info_sum, template_pos_idx, info_sum + 1)


for i in range(gray):
    final_circ.cswap(info_sum + 1, i, i + gray)


final_circ.barrier()
reset_origin_auxiliary(final_circ, [template_pos_idx, template_pos_idx - 1])
final_circ.barrier()
reset_origin_auxiliary(final_circ, [original_pos_idx + 1, original_pos_idx])
final_circ.barrier()



# final_circ = final_circ.compose(circ, range(0, 22), front=True)


# final_circ.measure_all()
final_circ.draw(output="mpl", filename="find_max.png", scale=0.5, fold=50)

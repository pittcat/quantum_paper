#!/usr/bin/env python
# -*- coding: utf-8 -*-

from qiskit.providers.aer import QasmSimulator
from qiskit import QuantumRegister, QuantumCircuit

from qiskit import transpile

num1, num2, res = 1, 1, 1
qn1 = QuantumRegister(num1, 'qn1')
qn2 = QuantumRegister(num2, 'qn2')
res = QuantumRegister(res, 'res')
circ = QuantumCircuit(qn1, qn2, res)

circ.h(qn1)
circ.h(qn2)

circ.x(qn1)
circ.ccx(qn1, qn2, res)
circ.x(qn1)

circ.draw(output='mpl', filename='circuit-debug.png', scale=0.5, fold=50)
circ.measure_all()

backend = QasmSimulator()
circ_compiled = transpile(circ, backend)
job_sim = backend.run(circ, shots=10)
result_sim = job_sim.result()
counts = result_sim.get_counts(circ_compiled)
counts_lst = [i[::-1] for i in counts.keys()]

print(counts_lst)
print(len(counts_lst))